import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import java.io.IOException;

public class TestBasketPriceCalculator {
    public static final String BASEURL="http://localhost:9095/getsearchedproductprice?name=cheese";
    HttpClient client= HttpClientBuilder.create().build();

    @Test
    public void baseUrlReturns200() throws IOException, JSONException {

        HttpGet get=new HttpGet(BASEURL);
        HttpResponse response=client.execute(get);
        int actualCode=response.getStatusLine().getStatusCode();
        String jsoBody= EntityUtils.toString(response.getEntity());
        System.out.println(jsoBody);

        JSONObject jsonObject=new JSONObject(jsoBody.toString());
        double val= (double) jsonObject.get("date");
        Assert.assertEquals(actualCode,200);
    }


}
