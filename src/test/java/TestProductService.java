import com.sdettest.exception.RecordNotFountException;
import com.sdettest.repository.ProductRepository;
import com.sdettest.service.ProductService;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

public class TestProductService {

    @Test
    public void testSumOfProductPrice(){
        ProductRepository mockRepo=mock(ProductRepository.class);
        Mockito.when(mockRepo.queryForSumOfTheColumn()).thenReturn(4.57);

        ProductService productService=new ProductService(mockRepo);
        double totalPrice=productService.queryForSumOfTheColumn();

        assertEquals(String.valueOf(4.57), String.valueOf(totalPrice));
    }

    @Test
    public void gatSumOfSearchedProductPrice() throws RecordNotFountException {
        ProductRepository mockRepo=mock(ProductRepository.class);
        Mockito.when(mockRepo.queryByNameAndPrice(anyString())).thenReturn(1.00);

        ProductService productService=new ProductService(mockRepo);
        double totalPrice=productService.queryByNameAndPrice("Bread");

        assertEquals(String.valueOf(1.00), String.valueOf(totalPrice));
    }

    @Test
    public void gatSumOfNonExistingSearchedProductPrice(){
        ProductRepository mockRepo=mock(ProductRepository.class);

        Mockito.when(mockRepo.queryByNameAndPrice(anyString())).thenReturn(0.0);

        ProductService productService=new ProductService(mockRepo);
        try{
        productService.queryByNameAndPrice("Sugar");
        fail("Product not found");}catch (Exception e){
            assertEquals("Product not fount",e.getMessage());
        }
    }
}
