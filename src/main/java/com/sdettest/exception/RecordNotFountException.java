package com.sdettest.exception;

public class RecordNotFountException extends Exception {
    public RecordNotFountException(String message){
        super(message);
    }
}
