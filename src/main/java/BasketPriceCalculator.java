import com.sdettest.model.Product;

import java.util.List;
import java.util.Optional;


/**
 * A class that takes in a list of products and a discounter object which will then return the total amount that a customer has to pay
 */
public class BasketPriceCalculator {
    /**
     * @param products The list of products the customer is buying
     * @param filter   Filters the name of the products being totalled up
     * @return A total basket price
     */
    public double calculatePrice(List<Product> products, Optional<String> filter) {
        return 0;
    }

}
